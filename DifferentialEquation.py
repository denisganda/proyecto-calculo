import sympy
from Operation import Operation

"""
Class to perform operations with differential equations.
"""


class DifferentialEquation:
    """
    Initializes the values of dx and dy.
    :param dx: Function that depends on x and y.
    :param dy: Function that depends on x and y.
    """

    def __init__(self, equation):
        self.operation = Operation(equation)
        self.equationOne = self.operation.findEquationOne(str(equation))
        self.equationTwo = self.operation.findEquationTwo(str(equation))
        self.dx = sympy.sympify(self.equationOne)
        self.dy = sympy.sympify(self.equationTwo)

    """
    Determine if the differential equation is exact.
    :return: True if the equation is exact, False otherwise.
    """

    def differential_equation_is_exact(self):
        if self.get_differential(self.dx, 'y') == self.get_differential(self.dy, 'x'):
            return True
        else:
            return False

    """
    Gets the value of dx.
    :return: The value of dx.
    """

    def get_dx(self):
        return self.dx

    """
    Gets the value of dy.
    :return: The value of dy.
    """

    def get_dy(self):

        return self.dy

    """
    Gets the derivate of the equation.
    Return: The value of the derivate.
    """

    def get_differential(self, equation, symbol):
        return sympy.diff(equation, sympy.symbols(symbol))

    """
    This method resolves the equation and print the progress.
    Return: If is exact, return the equation result.
    """

    def resolve(self):
        procedure = ""
        procedure += "Verify if the differential equation is exact: \n"
        procedure += "dM/dy: " + str(self.get_differential(self.dx, 'y')) + "\n"
        procedure += "dN/dx: " + str(self.get_differential(self.dy, 'x')) + "\n"
        isExact = self.differential_equation_is_exact()
        if isExact:
            M1 = self.operation.findM1(self.dx)
            N1 = self.operation.findN1(self.dy, self.get_differential(M1, 'y'))
            procedure += "\nFind M1:\n"
            procedure += "∫M(x,y)dx:\n"
            procedure += str(M1) + "\n"
            procedure += "Find N1:\n"
            procedure += "∫(N(x,y)-dM1/dy)dy:\n"
            procedure += str(N1) + "\n"

            result = M1 + N1
            procedure += "\nResult:\n"
            procedure += str(result)
            print(procedure)
            return procedure
        else:
            print("The differntial equation is not exact.")
            return "The differntial equation is not exact."
