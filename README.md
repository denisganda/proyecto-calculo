# Final Project - Calculus III

### Members
- Axel Javier Ayala Siles
- Denis Jorge Gandarillas Delgado
- Leonardo Alberto Herrera Rosales

### Dependencies
- Python 3
- Sympy
- PyQt5

### How to install dependencies
**Linux**  
Execute the next command lines in the terminal:
- sudo apt-get install python3-sympy
- sudo apt-get install python3-pyqt5
