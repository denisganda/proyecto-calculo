from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from DifferentialEquation import DifferentialEquation as DE
from ResultSheet import ResultSheet

class Calculator(QWidget):
    '''
    Initial function and help to create the project UI
    '''

    def __init__(self):
        super(Calculator, self).__init__()
        self.equation = ""

        self.setFixedSize(800, 1020)
        self.setWindowTitle("ECUACIONES DIFERENCIALES EXACTAS")
        self.setStyleSheet("background-color: #000000")

        self.inputCalculator = QLineEdit(self)
        self.inputCalculator.setGeometry(25, 25, 750, 200)
        self.inputCalculator.setStyleSheet(
            "background-color: #E8F9FD; font-size: 44px;")

        self.button1 = QPushButton("(", self)
        self.button1.setGeometry(40, 273, 100, 100)
        self.button1.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button1.clicked.connect(self.bracketsOne)

        self.button2 = QPushButton(")", self)
        self.button2.setGeometry(164, 273, 100, 100)
        self.button2.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button2.clicked.connect(self.bracketsTwo)

        self.button3 = QPushButton("x", self)
        self.button3.setGeometry(288, 273, 100, 100)
        self.button3.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button3.clicked.connect(self.xEquation)

        self.button4 = QPushButton("+", self)
        self.button4.setGeometry(412, 273, 100, 100)
        self.button4.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button4.clicked.connect(self.plus)

        self.deleteButton = QPushButton("<", self)
        self.deleteButton.setGeometry(536, 273, 224, 100)
        self.deleteButton.setStyleSheet(
            "background-color: #FF1E00; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.deleteButton.clicked.connect(self.deleteLast)

        self.button6 = QPushButton("^", self)
        self.button6.setGeometry(40, 394, 100, 100)
        self.button6.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button6.clicked.connect(self.exponent)

        self.button7 = QPushButton("√", self)
        self.button7.setGeometry(164, 394, 100, 100)
        self.button7.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button7.clicked.connect(self.square)

        self.button8 = QPushButton("y", self)
        self.button8.setGeometry(288, 394, 100, 100)
        self.button8.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button8.clicked.connect(self.yEquation)

        self.button9 = QPushButton("-", self)
        self.button9.setGeometry(412, 394, 100, 100)
        self.button9.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button9.clicked.connect(self.minus)

        self.button10 = QPushButton("dx", self)
        self.button10.setGeometry(536, 394, 100, 100)
        self.button10.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button10.clicked.connect(self.dx)

        self.button11 = QPushButton("dy", self)
        self.button11.setGeometry(660, 394, 100, 100)
        self.button11.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button11.clicked.connect(self.dy)

        self.button12 = QPushButton("1", self)
        self.button12.setGeometry(40, 515, 100, 100)
        self.button12.setStyleSheet(
            "background-color: #E48D5C; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button12.clicked.connect(self.one)

        self.button13 = QPushButton("2", self)
        self.button13.setGeometry(164, 515, 100, 100)
        self.button13.setStyleSheet(
            "background-color: #E48D5C; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button13.clicked.connect(self.two)

        self.button14 = QPushButton("3", self)
        self.button14.setGeometry(288, 515, 100, 100)
        self.button14.setStyleSheet(
            "background-color: #E48D5C; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button14.clicked.connect(self.three)

        self.button15 = QPushButton("*", self)
        self.button15.setGeometry(412, 515, 100, 100)
        self.button15.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button15.clicked.connect(self.multiplication)

        self.button16 = QPushButton("sen", self)
        self.button16.setGeometry(536, 515, 100, 100)
        self.button16.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 58px; border: none; border-radius: 50%;")
        self.button16.clicked.connect(self.sen)

        self.button17 = QPushButton("cos", self)
        self.button17.setGeometry(660, 515, 100, 100)
        self.button17.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 58px; border: none; border-radius: 50%;")
        self.button17.clicked.connect(self.cos)

        self.button18 = QPushButton("4", self)
        self.button18.setGeometry(40, 636, 100, 100)
        self.button18.setStyleSheet(
            "background-color: #E48D5C; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button18.clicked.connect(self.four)

        self.button19 = QPushButton("5", self)
        self.button19.setGeometry(164, 636, 100, 100)
        self.button19.setStyleSheet(
            "background-color: #E48D5C; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button19.clicked.connect(self.five)

        self.button20 = QPushButton("6", self)
        self.button20.setGeometry(288, 636, 100, 100)
        self.button20.setStyleSheet(
            "background-color: #E48D5C; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button20.clicked.connect(self.six)

        self.button21 = QPushButton("/", self)
        self.button21.setGeometry(412, 636, 100, 100)
        self.button21.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button21.clicked.connect(self.division)

        self.button22 = QPushButton("tan", self)
        self.button22.setGeometry(536, 636, 100, 100)
        self.button22.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 58px; border: none; border-radius: 50%;")
        self.button22.clicked.connect(self.tan)

        self.button23 = QPushButton("ln", self)
        self.button23.setGeometry(660, 636, 100, 100)
        self.button23.setStyleSheet(
            "background-color: #5CB8E4; color: #ffffff; font-size: 58px; border: none; border-radius: 50%;")
        self.button23.clicked.connect(self.ln)

        self.button24 = QPushButton("7", self)
        self.button24.setGeometry(40, 757, 100, 100)
        self.button24.setStyleSheet(
            "background-color: #E48D5C; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button24.clicked.connect(self.seven)

        self.button25 = QPushButton("8", self)
        self.button25.setGeometry(164, 757, 100, 100)
        self.button25.setStyleSheet(
            "background-color: #E48D5C; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button25.clicked.connect(self.eight)

        self.button26 = QPushButton("9", self)
        self.button26.setGeometry(288, 757, 100, 100)
        self.button26.setStyleSheet(
            "background-color: #E48D5C; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button26.clicked.connect(self.nine)

        self.button27 = QPushButton("0", self)
        self.button27.setGeometry(412, 757, 100, 100)
        self.button27.setStyleSheet(
            "background-color: #E48D5C; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.button27.clicked.connect(self.zero)

        self.equalsButton = QPushButton("=", self)
        self.equalsButton.setGeometry(536, 757, 224, 100)
        self.equalsButton.setStyleSheet(
            "background-color: #AD00FF; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")
        self.equalsButton.clicked.connect(self.equals)

        self.deleteAllButton = QPushButton("DELETE ALL", self)
        self.deleteAllButton.setGeometry(40, 878, 348, 100)
        self.deleteAllButton.setStyleSheet(
            "background-color: #FF1E00; color: #ffffff; font-size: 44px; border: none; border-radius: 50%;")
        self.deleteAllButton.clicked.connect(self.deleteAll)

        self.resolveButton = QPushButton("RESOLVE", self)
        self.resolveButton.setGeometry(412, 878, 348, 100)
        self.resolveButton.setStyleSheet(
            "background-color: #59CE8F; color: #ffffff; font-size: 44px; border: none; border-radius: 50%;")
        self.resolveButton.clicked.connect(self.result)

        """
        This function helps to add the parenthesis symbol to the equation
        """

    def bracketsOne(self):
        equation = self.inputCalculator.text() + "("
        self.inputCalculator.setText(equation)
        self.equation += "("

        """
        This function helps to add the parenthesis symbol to the equation
        """

    def bracketsTwo(self):
        equation = self.inputCalculator.text() + ")"
        self.inputCalculator.setText(equation)
        self.equation += ")"

        """
        This function helps to add the x symbol to the equation
        """

    def xEquation(self):
        equation = self.inputCalculator.text() + "x"
        self.inputCalculator.setText(equation)
        self.equation += "x"

        """
        This function helps to add the + symbol to the equation
        """

    def plus(self):
        equation = self.inputCalculator.text() + "+"
        self.inputCalculator.setText(equation)
        self.equation += "+"

        """
        This function eliminates the entire contents of the equation.
        """

    def equals(self):
        equation = self.inputCalculator.text() + "="
        self.inputCalculator.setText(equation)
        self.equation += "="

        """
        This function helps to add the ^ symbol to the equation
        """

    def exponent(self):
        equation = self.inputCalculator.text() + "^"
        self.inputCalculator.setText(equation)
        self.equation += "**"

        """
        This function helps to add the √ symbol to the equation
        """

    def square(self):
        equation = self.inputCalculator.text() + "√("
        self.inputCalculator.setText(equation)
        self.equation += "sqrt("

        """
        This function helps to add the y symbol to the equation
        """

    def yEquation(self):
        equation = self.inputCalculator.text() + "y"
        self.inputCalculator.setText(equation)
        self.equation += "y"

        """
        This function helps to add the - symbol to the equation
        """

    def minus(self):
        equation = self.inputCalculator.text() + "-"
        self.inputCalculator.setText(equation)
        self.equation += "-"

        """
        This function helps to add the dy symbol to the equation
        """

    def dy(self):
        equation = self.inputCalculator.text() + "dy"
        self.inputCalculator.setText(equation)
        self.equation += "dy"

        """
        This function helps to add the dx symbol to the equation
        """

    def dx(self):
        equation = self.inputCalculator.text() + "dx"
        self.inputCalculator.setText(equation)
        self.equation += "dx"

        """
        This function helps to add the number 1 to the equation
        """

    def one(self):
        equation = self.inputCalculator.text() + "1"
        self.inputCalculator.setText(equation)
        self.equation += "1"

        """
        This function helps to add the number 2 to the equation
        """

    def two(self):
        equation = self.inputCalculator.text() + "2"
        self.inputCalculator.setText(equation)
        self.equation += "2"

        """
        This function helps to add the number 3 to the equation
        """

    def three(self):
        equation = self.inputCalculator.text() + "3"
        self.inputCalculator.setText(equation)
        self.equation += "3"

        """
        This function helps to add the * symbol to the equation
        """

    def multiplication(self):
        equation = self.inputCalculator.text() + "*"
        self.inputCalculator.setText(equation)
        self.equation += "*"

        """
        This function helps to add the sen symbol to the equation
        """

    def sen(self):
        equation = self.inputCalculator.text() + "sen("
        self.inputCalculator.setText(equation)
        self.equation += "sin("

        """
        This function helps to add the cos symbol to the equation
        """

    def cos(self):
        equation = self.inputCalculator.text() + "cos("
        self.inputCalculator.setText(equation)
        self.equation += "cos("

        """
        This function helps to add the number 4 to the equation
        """

    def four(self):
        equation = self.inputCalculator.text() + "4"
        self.inputCalculator.setText(equation)
        self.equation += "4"

        """
        This function helps to add the number 5 to the equation
        """

    def five(self):
        equation = self.inputCalculator.text() + "5"
        self.inputCalculator.setText(equation)
        self.equation += "5"

        """
        This function helps to add the number 6 to the equation
        """

    def six(self):
        equation = self.inputCalculator.text() + "6"
        self.inputCalculator.setText(equation)
        self.equation += "6"

        """
        This function helps to add the number 7 to the equation
        """

    def seven(self):
        equation = self.inputCalculator.text() + "7"
        self.inputCalculator.setText(equation)
        self.equation += "7"

        """
        This function helps to add the number 8 to the equation
        """

    def eight(self):
        equation = self.inputCalculator.text() + "8"
        self.inputCalculator.setText(equation)
        self.equation += "8"

        """
        This function helps to add the number 9 to the equation
        """

    def nine(self):
        equation = self.inputCalculator.text() + "9"
        self.inputCalculator.setText(equation)
        self.equation += "9"

        """
        This function helps to add the number 0 to the equation
        """

    def zero(self):
        equation = self.inputCalculator.text() + "0"
        self.inputCalculator.setText(equation)
        self.equation += "0"

        """
        This function helps to add the / symbol to the equation
        """

    def division(self):
        equation = self.inputCalculator.text() + "/"
        self.inputCalculator.setText(equation)
        self.equation += "/"
        self.sen()

        """
        This function helps to add the tan symbol to the equation
        """

    def tan(self):
        equation = self.inputCalculator.text() + "tan("
        self.inputCalculator.setText(equation)
        self.equation += "tan("

        """
        This function helps to add the ln symbol to the equation
        """

    def ln(self):
        equation = self.inputCalculator.text() + "ln"
        self.inputCalculator.setText(equation)
        self.equation += "ln("

        """
        This function helps to perform the solution of the equation.
        """

    def result(self):
        if self.inputCalculator.text() == "EMPTY" or self.inputCalculator.text() == "EMPT" or self.inputCalculator.text() == "EMP" or self.inputCalculator.text() == "EM" or self.inputCalculator.text() == "E":
            self.inputCalculator.setText("")

        if self.inputCalculator.text() != "":
            self.equation = self.inputCalculator.text()
            print(self.equation)
            de = DE(self.equation)
            return de.resolve()
        else:
            self.inputCalculator.setText("EMPTY")

    """
    This function helps to delete the last element of the equation.
    """

    def deleteLast(self):
        equation = self.inputCalculator.text()[:-1]
        self.inputCalculator.setText(equation)
        self.equation = self.equation[:-1]

    def deleteAll(self):
        self.equation = ""
        self.inputCalculator.setText("")
