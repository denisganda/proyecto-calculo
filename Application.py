from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from ResultSheet import ResultSheet
from Calculator import Calculator


class Application(QMainWindow):
    def __init__(self):
        super(Application, self).__init__()
        self.widgets = QStackedWidget(self)
        self.resultPage = ResultSheet()
        self.calculatorPage = Calculator()
        self.widgets.addWidget(self.calculatorPage)
        self.widgets.addWidget(self.resultPage)

        self.setCentralWidget(self.widgets)
        self.widgets.setCurrentWidget(self.calculatorPage)
        self.init()

    def init(self):
        self.resize(800, 900)
        self.setWindowTitle("ECUACIONES DIFERENCIALES EXACTAS")
        self.setStyleSheet("background-color: #000000;")
        self.connections()

    def connections(self):
        self.calculatorPage.resolveButton.clicked.connect(self.changeResultPage)
        self.resultPage.backButton.clicked.connect(self.changeCalculatorPage)

    def changeCalculatorPage(self):
        self.widgets.setCurrentWidget(self.calculatorPage)

    def changeResultPage(self):
        self.resultPage.outputResult.setText(str(self.calculatorPage.result()))
        self.widgets.setCurrentWidget(self.resultPage)


if __name__ == '__main__':
    app = QApplication([])
    window = Application()
    window.show()
    app.exec_()
