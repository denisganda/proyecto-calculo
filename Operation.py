from sympy import *
from sympy.abc import x, y


class Operation:
    """
    Constructor method
    """
    def __init__(self, equation):
        self.sizeEO = 0

        equationOne = self.findEquationOne(equation)

        # Obtener la segunda ecuación
        equationTwo = self.findEquationTwo(equation)

        # Calcular la integral de M
        integralM = self.findM1(equationOne)

        # Calcular la derivada de la integral de M con respecto a y
        derivativeM = self.findDerivative(integralM, 'y')

        # Restar la derivada de la integral de M a la segunda ecuación
        equationThree = simplify(sympify(equationTwo) - derivativeM)

        # Calcular la integral de la nueva ecuación obtenida
        integralN = self.findN1(equationThree, integralM)

    """
    This function helps to find the derivative

    :param equation: Equation to solve
    :type equation: int
    :param symbol: Symbol by which to derive
    :type symbol: String
    """
    def findDerivative(self, equation, symbol):
        symbol = symbols(symbol)
        return diff(equation, symbol)

    """
    This function helps to find the integral factor of M
    
    :param equation: Equation to solve
    :type equation: int
    """
    def findM1(self, functionM):
        return integrate(functionM, x)

    """
    This function helps to find the integral factor of N
    
    :param equation: Equation to solve
    :type equation: int
    :param equationM: It is an integral factor of M
    :type equation: int
    """
    def findN1(self, functionN, M1):
        integral = integrate((functionN-M1), y)
        return integral

    def findEquationOne(self, equation):
        equationOne = ""
        for index in range(len(equation)):
            if (equation[index] != "d"):
                equationOne += equation[index]
            else:
                if (equation[index+1] == "x"):
                    self.sizeEO = index+1
                    break                
        return equationOne

    def findEquationTwo(self, equation):
        sizeEquationOne = self.sizeEO
        equationTwo = ""
        
        for index in range(sizeEquationOne+2, len(equation)):
            if (equation[index] != "d"):
                equationTwo += equation[index]
            else:
                if (equation[index+1] == "y"):
                    break        
        return equationTwo

