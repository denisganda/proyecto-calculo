from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class ResultSheet(QWidget):
    def __init__(self):
        super(ResultSheet, self).__init__()
        self.result = ""

        self.setFixedSize(800, 1020)
        self.setWindowTitle("RESULT")

        self.outputResult = QLabel(self)
        self.outputResult.setGeometry(30, 105, 740, 850)
        self.outputResult.setText(self.result)
        self.outputResult.setStyleSheet(
            "background-color: #fff5d6; color: #000000; font-size: 32px; padding: 8px; border-radius: 10%;")

        self.backButton = QPushButton("←", self)
        self.backButton.setGeometry(10, 5, 100, 100)
        self.backButton.setStyleSheet(
            "background-color: transparent; color: #ffffff; font-size: 64px; border: none; border-radius: 50%;")

    def setResult(self, result):
        self.result = result
