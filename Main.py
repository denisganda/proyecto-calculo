import DifferentialEquation as ed

equation = ed.DifferentialEquation("(x**3+x*y**2)dx+(x**2*y+y**3)dy=0")
equation.resolve()

print("\n---------------------------\n")
equation = ed.DifferentialEquation("(3*x**2+4*x*y)dx+(2*x**2+2*y)dy=0")
equation.resolve()

print("\n---------------------------\n")
equation = ed.DifferentialEquation("(2*x-1)dx=(3*y+7)dy")
equation.resolve()

print("\n---------------------------\n")
equation = ed.DifferentialEquation("(4*x**4+2*x*y)dx+(2*x**2+2*y)dy=0")
equation.resolve()